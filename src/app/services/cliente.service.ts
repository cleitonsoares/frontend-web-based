import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Cliente } from '../cliente/Cliente';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class ClienteService {

    urlApi: string = 'http://localhost:8081/slim/v1';

    constructor(private http: Http) { }

    /**
     * Requisição responsável por listar os clientes
     */
    getClientes(): Observable<Cliente> {

        return this.http.get(this.urlApi + '/clientes')
            .map((response: Response) => <Cliente>response.json())
            .do(data => '')
            .catch(this.handleError);
    }

    /**
     * Requisição responsável por buscar um determinado cliente
     * @param cod_cliente
     */
    getCliente(cod_cliente: number): Observable<Cliente> {

        return this.http.get(this.urlApi + '/cliente/' + cod_cliente)
            .map((response: Response) => <Cliente>response.json())
            .do(data => '')
            .catch(this.handleError);
    }

    /**
     * Busca pelos dados de faixa etária para montagem do gráfico
     * @returns {any|Promise<R>|Maybe<T>}
     */
    getFaixaEtaria(): Observable<string> {

        return this.http.get(this.urlApi + '/faixa-etaria')
            .map((response: Response) => <string>response.json())
            .do(data => '')
            .catch(this.handleError);
    }

    /**
     * Bueca pelos dados do pubico do sistema para montagem do gráfico
     * @returns {Maybe<T>|any|Promise<R>}
     */
    getPublico(): Observable<string> {

        return this.http.get(this.urlApi + '/publico')
            .map((response: Response) => <string>response.json())
            .do(data => '')
            .catch(this.handleError);
    }

    /**
     * Salva ou atualiza o registro de um cliente
     * @param cliente
     * @returns {any|Promise<R>|Maybe<T>}
     */
    salvarCliente(cliente: Cliente): Observable<string> {

        console.log('cliente', cliente);

        return this.http.post(this.urlApi + '/cliente', cliente)
            .map((response: Response) => <string>response.json())
            .do(data => '')
            .catch(this.handleError);

    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
