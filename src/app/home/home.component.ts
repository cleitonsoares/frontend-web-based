import { Component, OnInit } from '@angular/core';

import { ClienteService } from "../services/cliente.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public barChartOptions: any;
    public barChartLabelsFaixaEtaria:string[];
    public barChartLabelsPublico:string[];
    public barChartDataFaixaEtaria:any[];
    public barChartDataPublico:any[];

    public instalacao:boolean     = false;
    public barChartType:string    = 'bar';
    public barChartLegend:boolean = true;

    constructor(public clienteService: ClienteService) {

        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };

        this.barChartLabelsFaixaEtaria = ['0 a 9', '10 a 19', '20 a 29', '30 a 39', 'Maior do que 40'];

        this.barChartLabelsPublico = ['Masculino', 'Feminino'];

        this.barChartDataFaixaEtaria = [
            {data: [0, 0, 0, 0, 0], label: 'Faixa Etária'},
        ];
        this.barChartDataPublico = [
            {data: [0, 0], label: 'Publico'},
        ];
    }

    getFaixaEtaria() {
        this.clienteService.getFaixaEtaria()
            .subscribe((data: string) => this.retornoFaixaEtaria(data),
                error => this.setInstalacao(error)
            );
    }

    retornoFaixaEtaria(data) {

        this.resetEstatisticas();

        if(data.length) {

            this.barChartDataFaixaEtaria = [
                {data: [data[0].faixa0_9, data[0].faixa10_19, data[0].faixa20_29, data[0].faixa30_39, data[0].faixa40], label: 'Faixa Etária'}
            ];

            console.log('this.barChartDataFaixaEtaria', this.barChartDataFaixaEtaria)
        }
    }

    getPublico() {
        this.clienteService.getPublico()
            .subscribe((data: string) => this.retornoPublico(data),
                error => this.setInstalacao(error)
            );
    }

    retornoPublico(data) {

        this.resetEstatisticas();

        if(data.length) {

            this.barChartDataPublico = [
                {data: [data[0].masc, data[0].fem], label: 'Publico'}
            ];

            console.log('this.barChartDataFaixaEtaria', this.barChartDataFaixaEtaria)
        }
    }

    resetEstatisticas() {
        this.barChartDataFaixaEtaria = [
            {data: [0, 0, 0, 0, 0], label: 'Faixa Etária'},
        ];

        this.barChartDataPublico = [
            {data: [0, 0], label: 'Publico'},
        ];
    }

    setInstalacao(error) {
        this.instalacao = true;
        console.log('Erro!', error)
    }


    ngOnInit() {

        this.getFaixaEtaria();
    }

}
