export class Cliente{

    constructor(
        public cod_cliente: number,
        public dat_nascimento: string,
        public doc_cliente: number,
        public sexo_cliente: string,
        public end_cliente: string,
        public nome_cliente: string
    ) {  }
}