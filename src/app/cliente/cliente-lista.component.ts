import { Component, OnInit } from '@angular/core';

import { ClienteService } from "../services/cliente.service";
import { Cliente } from "./Cliente";

@Component({
    selector: 'app-cliente-lista',
    templateUrl: './cliente-lista.component.html',
    styleUrls: ['./cliente-lista.component.css']
})
export class ClienteListaComponent implements OnInit {

    arrClientes: any;
    exibeLista: boolean;

    constructor(private clienteService:ClienteService) { }

    ngOnInit() {

        this.exibeLista = false;

        this.clienteService.getClientes()
            .subscribe((data: Cliente) => this.arrClientes = data,
                error => console.log(error),
                () => this.handleExibeLista()
            );
    }

    handleExibeLista() {

        this.exibeLista = (this.arrClientes.length);
    }

}
