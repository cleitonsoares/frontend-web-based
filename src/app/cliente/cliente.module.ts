import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { ClienteComponent } from './cliente.component';
import { ClienteListaComponent } from './cliente-lista.component';
import { routing } from "../app.routing";
import { MaterializeModule } from "angular2-materialize/dist/materialize-module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        routing,
        MaterializeModule,
        HttpModule,
        BrowserModule,
        BrowserAnimationsModule,
        ToastModule.forRoot()
    ],
    declarations: [
        ClienteComponent,
        ClienteListaComponent
    ],
    exports: [
        ClienteComponent,
        ClienteListaComponent,
        BrowserModule,
        BrowserAnimationsModule,
        ToastModule
    ]
})
export class ClienteModule { }
