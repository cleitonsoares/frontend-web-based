import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from "rxjs/Rx";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ClienteService } from "../services/cliente.service";
import { Cliente } from "./Cliente";

@Component({
    selector: 'app-cliente',
    templateUrl: './cliente.component.html',
    styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit {

    routeListner: Subscription;

    nomeModulo: string;
    datePickerParams: any[];

    cliente: any;

    constructor(
        public clienteService: ClienteService,
        private route: ActivatedRoute,
        private router: Router,
        public toastr: ToastsManager,
        public vcr: ViewContainerRef
    ) {

        this.toastr.setRootViewContainerRef(vcr);
    }

    getCliente(params) {

        if(params['cod_cliente']) {

            this.clienteService.getCliente(params['cod_cliente'])
                .subscribe((data: Cliente) => this.cliente = data,
                    error => this.toastr.success(error, 'Erro!'),
                    () => this.nomeModulo = (this.cliente.length) ? 'Novo Cliente' : 'Editar Cliente'
                );

        } else {
            this.cliente = {};
        }
    }

    salvarCliente() {
        this.clienteService.salvarCliente(this.cliente)
            .subscribe((data: string) => this.retornoSalvarCliente(data),
                error => this.toastr.success(error, 'Erro!')
            );
    }

    retornoSalvarCliente(data) {

        if(data.cod_cliente) {

            this.router.navigate(['cliente',data.cod_cliente]);
            setTimeout(() => {
                this.toastr.success('Cadastro enviado com sucesso', '');
            }, 1000);
        }
    }

    onSubmit(form) {

        if(form.valid) {

            this.salvarCliente();
        }
    }

    ngOnInit() {

        this.resetVars();

        console.log('this.nomeModulo', this.nomeModulo)

        this.routeListner = this.route.params.subscribe(
            (params: any) => {
                this.getCliente(params)
            }
        );
    }

    ngOnDestroy() {
        this.routeListner.unsubscribe();
    }

    resetVars() {

        this.nomeModulo = 'Novo Cliente';
        this.cliente    = [{}];

        this.datePickerParams = [{
            monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
            weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            today: 'Hoje',
            clear: 'Limpar',
            close: 'Ok',
            labelMonthNext: 'Próximo mês',
            labelMonthPrev: 'Mês anterior',
            labelMonthSelect: 'Selecione um mês',
            labelYearSelect: 'Selecione um ano',
            closeOnSelect: true,
            selectMonths: true,
            selectYears: 200,
            format: 'dd/mm/yyyy'
        }];
    }
}
