/* Classe responsável pelo roteamento do projeto */

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { ClienteComponent } from "./cliente/cliente.component";
import { ClienteListaComponent } from "./cliente/cliente-lista.component";

const APP_ROUTES : Routes = [{
    path: '',
    component: HomeComponent
}, {
    path: 'clientes',
    component: ClienteListaComponent
}, {
    path: 'cliente',
    component: ClienteComponent
}, {
    path: 'cliente/:cod_cliente',
    component: ClienteComponent
}];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);