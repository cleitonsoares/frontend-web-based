import { MaterializeModule } from 'angular2-materialize';
import { routing } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppComponent } from './app.component';


import { ClienteModule } from './cliente/cliente.module';
import { ClienteService } from "./services/cliente.service";
import { HomeComponent } from './home/home.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent
    ],  
    imports: [
        MaterializeModule,
        BrowserModule,
        ClienteModule,
        routing,
        ChartsModule
    ],
    providers: [ ClienteService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
