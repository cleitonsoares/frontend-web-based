<?php
use Illuminate\Database\Capsule\Manager as Capsule;

class Cliente extends Illuminate\Database\Eloquent\Model
{
    protected $table      = 'cliente';
    protected $primaryKey = 'cod_cliente';
    public    $timestamps = false;

    /**
     * Retorna o contador com a faixa etária dos clientes
     * @return mixed
     */
    public static function getFaixaEtaria()
    {

        $results = Capsule::select('select sum(case when YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(dat_nascimento))) between 0 and 9 then 1 else 0 end) as faixa0_9,
                                           sum(case when YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(dat_nascimento))) between 10 and 19 then 1 else 0 end) as faixa10_19,
                                           sum(case when YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(dat_nascimento))) between 20 and 29 then 1 else 0 end) as faixa20_29,
                                           sum(case when YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(dat_nascimento))) between 30 and 39 then 1 else 0 end) as faixa30_39,
                                           sum(case when YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(dat_nascimento))) > 40 then 1 else 0 end) as faixa40
                                    from cliente');

        return $results;
    }

    /**
     * Retorna o contador com o público do cliente
     * @return mixed
     */
    public static function getPublico()
    {

        $results = Capsule::select('select sum(case when sexo_cliente = "F" then 1 else 0 end) as fem,
                                           sum(case when sexo_cliente = "M" then 1 else 0 end) as masc
                                    from cliente');

        return $results;
    }
}