<?php

use Slim\Http\Request;
use Slim\Http\Response;

// CORS
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

/**
 * Lista os registros de cliente:
 */
$app->get('/clientes', function ($request, $response, $args) {

    $objCliente  = Cliente::all();
    $arrClientes = [];

    if($objCliente) {
        $arrClientes = $objCliente->toArray();

        foreach ($arrClientes as $k => $cliente)
        {

            $arrClientes[$k]['dat_nascimento'] = Formata::toDateBr($cliente['dat_nascimento']);
        }
    }

    return $response->withJson($arrClientes, 200);
});

/**
 * Busca um registro específico:
 */
$app->get('/cliente/{id}', function ($request, $response, $args) {

    $arrCliente = [];

    # Pega o parametro passado na URL:
    $codCliente = $args['id'];

    # Busca o registro:
    $objCliente = Cliente::find($codCliente);

    if($objCliente) {

        $arrCliente = $objCliente->toArray();
        $arrCliente['dat_nascimento'] = Formata::toDateBr($arrCliente['dat_nascimento']);
    }

    # Retorna a rquisição com o registro:
    return $response->withJson($arrCliente, 200);
});

/**
 * Busca os dados de faixa etária para o dashboard:
 */
$app->get('/faixa-etaria', function ($request, $response, $args) {

    # Busca o registro:
    $arrFaixaEtaria = Cliente::getFaixaEtaria();

    # Retorna a rquisição com o registro:
    return $response->withJson($arrFaixaEtaria, 200);
});

/**
 * Busca os dados do publico para o dashboard:
 */
$app->get('/publico', function ($request, $response, $args) {

    # Busca o registro:
    $arrPublico = Cliente::getPublico();

    # Retorna a rquisição com o registro:
    return $response->withJson($arrPublico, 200);
});


/**
 * Atualiza ou insre um  novo registro conforme o corpo da rquisição:
 */
$app->post('/cliente', function ($request, $response, $args) {

    # Pega os parametros passados no corpo da requisição:
    $arrParam = $request->getParsedBody();

    $arrParam['dat_nascimento'] = Formata::toDate($arrParam['dat_nascimento']);

    # Caso tenha a posição código do cliente, busca seu registro, caso contrário
    # instancia um novo:
    if (isset($arrParam['cod_cliente'])) {

        $cliente = Cliente::find($arrParam['cod_cliente']);
    } else {

        $cliente = new Cliente();
    }

    # Monta o objeto com os dados do cliente a ser atualizado/inserido:
    $cliente->nome_cliente   = isset($arrParam['nome_cliente'])   ? $arrParam['nome_cliente']   : null;
    $cliente->dat_nascimento = isset($arrParam['dat_nascimento']) ? $arrParam['dat_nascimento'] : null;
    $cliente->doc_cliente    = isset($arrParam['doc_cliente'])    ? $arrParam['doc_cliente']    : null;
    $cliente->end_cliente    = isset($arrParam['end_cliente'])    ? $arrParam['end_cliente']    : null;
    $cliente->sexo_cliente   = isset($arrParam['sexo_cliente'])   ? $arrParam['sexo_cliente']   : null;

    # Salva o registro:
    $cliente->save();

    return $response->withJson($cliente, 200);
});