<?php
use Illuminate\Database\Capsule\Manager as Capsule;

// Database configuration
$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'nep_sistema',
    'username'  => 'root',
    'password'  => '123',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
]);

$capsule->bootEloquent();
$capsule->setAsGlobal();