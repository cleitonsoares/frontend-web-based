<?php

class Formata
{

    static public function toDate($data = '')
    {
        list($dia,$mes,$ano) = explode('/',@$data);
        return $ano.'-'.$mes.'-'.$dia;
    }

    static public function toDateBr($data = '')
    {
        if(!empty($data) AND $data!='0000-00-00') {

            list($ano,$mes,$dia) = explode('-',substr(@$data, 0, 10));
            return $dia.'/'.$mes.'/'.$ano;
        }   
        return 0;

    }
}